//
//  ContentView.swift
//  Moonshot
//
//  Created by Skyler Bellwood on 7/10/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State private var isShowingDates = true
    
    let astronauts: [Astronaut] = Bundle.main.decode("astronauts.json")
    let missions: [Mission] = Bundle.main.decode("missions.json")
    
    var body: some View {
        NavigationView {
            List(missions) { mission in
                NavigationLink(destination: MissionView(mission: mission, astronauts: self.astronauts)) {
                    Image(mission.image)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 44, height: 44)
                    
                    VStack(alignment: .leading) {
                        Text(mission.displayName)
                            .font(.headline)
                        
                        if self.isShowingDates {
                            Text(mission.formattedLaunchDate)
                        } else {
                            ForEach(mission.crew, id: \.name) { crew in
                                Text("\(crew.name.capitalized): \(crew.role.capitalized)")
                            }
                        }
                        
                    }
                }
            }
            .navigationBarTitle("Moonshot")
            .navigationBarItems(trailing:
                Button(isShowingDates ? "Show Astronauts" : "Show Launch Dates") {
                    self.isShowingDates.toggle()
                }
            )
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
