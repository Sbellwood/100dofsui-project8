//
//  AstronautView.swift
//  Moonshot
//
//  Created by Skyler Bellwood on 7/23/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import SwiftUI

struct AstronautView: View {
    
    let astronaut: Astronaut
    var missions: [Mission]
    
    init(astronaut: Astronaut) {
        self.astronaut = astronaut
        
        let allMissions: [Mission] = Bundle.main.decode("missions.json")
        self.missions = [Mission]()
        
        for mission in allMissions {
            if mission.crew.first(where: { $0.name == self.astronaut.id }) != nil {
                self.missions.append(mission)
            }
        }
        
    }
    
    var body: some View {
        GeometryReader { geometry in
            ScrollView(.vertical) {
                VStack {
                    Image(self.astronaut.id)
                        .resizable()
                        .scaledToFit()
                        .frame(width: geometry.size.width)
                    
                    Text(self.astronaut.description)
                        .padding()
                        .layoutPriority(1)
                    
                    Text("Missions")
                        .font(.headline)
                    
                    ForEach(self.missions, id: \.id) { mission in
                        HStack {
                            Text("\(mission.displayName)")
                                .padding(.horizontal)
                            
                            Spacer()
                        }
                        
                    }
                }
            }
        }
        .navigationBarTitle(Text(astronaut.name), displayMode: .inline)
    }
}

struct AstronautView_Previews: PreviewProvider {
    static let astronauts: [Astronaut] = Bundle.main.decode("astronauts.json")
    
    static var previews: some View {
        AstronautView(astronaut: astronauts[0])
    }
}
