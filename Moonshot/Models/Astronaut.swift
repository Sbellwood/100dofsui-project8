//
//  Astronaut.swift
//  Moonshot
//
//  Created by Skyler Bellwood on 7/22/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import Foundation

struct Astronaut: Codable, Identifiable {
    let id: String
    let name: String
    let description: String
}
